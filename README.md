# ShirazLUG Resources repo

Files, presentations and other resources are dumped in this repository.


## presentations
this is where the files for each presentation gets saved.
if you want to get a single file, you can use curl.

for examlpe:
> curl 'https://gitlab.com/shirazlug/resources/raw/master/presentations/session_122/session_122.pdf'

## logo
svg, png format of shiruzLUG logos in different colors.

## pixel
modified version of the logos,circled for printing on pixels!